Proyecto del curso “Universidad Spring 2021 – Spring Framework y Spring Boot” (UDEMY)
Inicia con introducción a Hibernate para el manejo de la capa de datos.

El objetivo de este ejercicio fue realizar una aplicación para gestionar un sistema de alumnos de una Universidad X, que debe contener 
por cada alumno sus datos de contacto, nombre, apellido, cursos asignados y turnos. 
La idea principal es trabajar más el manejo de la base de datos con Hibernate, cuya ventaja es que permite recuperar objetos completos
desde la base de datos, y no columnas aisladas.

Desarrollado con Apache Netbeans, utilizando Java Empresarial (JavaEE version 8), y JDK 1.8.
Administrador de librerías Maven.
Base de datos utilizada MySQL 8 con MySQL Workbench.
Capa de datos utilizando Hibernate.
Todas estas dependencias del proyecto descargadas mediante la configuración del archivo pom.xml.

La configuración de conexión a la base de datos está realizada en el archivo persistence.xml (hibernate-jpa-sistema-gestion-alumnos/PracticaSGA/src/main/resources/META-INF), 
declarando también la unidad de persistencia y las clases, o "java beans", que serán utilizadas.
En este archivo también se realiza la conexión por medio de JDBC y Mysql Driver.
MySQL en localhost:3306 .

El servidor de aplicaciones es Glassfish, versión 5.0.
Servidor en localhost:8080 .

También está la configuración de Log4j , para el manejo del log y así poder enviar mensajesen la consola de Glassfish  
(a nivel INFO y a nivel DEBUG).
El archivo de configuración se encuentra en src/main/resources/log4j2.xml . 


Desarrollo de la aplicación con sus tres capas: capa de datos, capa de negocio y capa web.
Más las clases de modelo o dominio del problema.

Las clases de dominio o modelo están definidas como @Entity para que JPA pueda relacionarlas con los registros de la base de datos.
Cada objeto tiene su atributo id (o llave primaria de acceso), que será creada de manera automática y autoincrementable desde la base de datos por
cada registro creado (de ahí la anotación @GeneratedValue en cada una de las clases de dominio).
También se maneja aquí el concepto de base de datos relacional y asociaciones (OneToOne, OneToMany, ManyToOne).
De esta manera también aplica el concepto de "persistencia en cascada", de esta manera, si se afecta un registro también serán
afectados los registros relacionados a ese objeto que se ejecute en ese momento (en el caso que sean modificados también).
Cada una de estas clases de entidad contienen los métodos "get" y "set" de cada atributo, más el método toString.

La tabla o clase Asignaciones es transitoria. 
Dado que la relación entre Alumno y Curso es de "ManyToMany" (dado que muchos alumnos pueden tener muchos cursos), la idea entonces es romper con
la asosociación de muchos a muchos, para que sea más limpio el código.
Asignaciones contiene las llaves foráneas de Alumno y Curso, y así estas clases tienen una relación de "OneToMany" con Asignaciones (un Alumno
muchas Asignaciones, un Curso muchas Asignaciones).

En la capa de datos, dentro de la ruta hibernate-jpa-sistema-gestion-alumnos/PracticaSGA/src/main/java/ar/com/nmv/dao, 
se encuentran todas las clases DAO de cada entidad.
La clase GenericDao se encarga de crear el objeto EntityManager y así poder llamar a este objeto en cada DAO (método getEntityManager, mediante el
EntityManagerFactory).
En las clases DAO se encuentran los métodos para interactuar con la base de datos: get(mediante el método getResultList
del objeto Query, creado en el mismo método), persist (para insertar un nuevo registro), merge (para actualizar un registro) y delete (realizando
primero un merge del objeto, y luego delete).

Manejo de transacciones de manera manual, iniciando la transacción (getTransaction.begin()), para realizar un commit en el caso de éxito
o un rollback en el caso de fallo (es decir, que dentro de la transacción, si falla una de las operaciones, ninguna de estas operaciones
será guardada en la basede datos).

En la capa de negocio se encuentran los métodos para interactuar con la capa de datos.
Se crea una instancia de la clase DAO para poder acceder a sus métodos.

La capa Web está desarrollada siguiendo el patrón de diseño MVC (Model-View-Controller).
Esta contiene a los Servlets, que reciben las peticiones del cliente por medio de las vistas (JSP´s), y a su vez se conecta con la capa de 
negocio y con las clases de modelo.
Dentro de la carpeta src/main/java/ar/com/nmv/web, se encuentran los Servlets, clases que extienden de la interface HttpServlet, para el 
manejo de las peticiones (request), las respuestas (response).
Tanto para agregar registros, modificarlos, eliminarlos, recuperar datos y redireccionar entre los diferentes JSP´s (en algunos casos, realizando
un "forward" con la petición y la respuesta).
Método doGet para links, método doPost para formularios.
Cada Servlet cuenta con su path para poder acceder desde el JSP.

En la ruta src/main/webapp/ se encuentra el archivo index.jsp, y dentro de src/main/webapp/WEB-INF se encuentran los JSP´s a desplegar 
para el cliente.
Los JSP´s permiten utilizar "expression languaje" con código Java, para un mejor manejo de los datos de los componentes Java Beans.
En el JSP de listarAlumnos se utiliza además el concepto de "taglibs", con etiquetas especiales para trabajar sobre las vistas. En este caso,
se importan los tags de Core mediante el prefijo "c", lo que permite contar con algunas funciones como, por ejemplo, iterar una lista
dentro del mismo JSP.

Se incluye una clase llamada TestDao, en la carpeta de Test, con un método Main para simular un cliente y realizar pruebas para chequear
que esté bien configurada la conexión a la base de datos y la persistencia en cascada.










