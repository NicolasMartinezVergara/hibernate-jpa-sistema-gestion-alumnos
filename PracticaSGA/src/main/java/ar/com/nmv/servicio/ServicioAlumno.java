package ar.com.nmv.servicio;

import ar.com.nmv.dao.AlumnoDao;
import ar.com.nmv.domain.Alumno;
import java.util.List;

public class ServicioAlumno {
    
    private AlumnoDao alumnoDao = new AlumnoDao();
    
    public List<Alumno> listarAlumnos() {
        return alumnoDao.listar();        
    }
    
    public void guardarAlumno(Alumno alumno) {
        if (alumno != null && alumno.getIdAlumno() == null) {
            alumnoDao.insertar(alumno);
        } else {
            alumnoDao.actualizar(alumno);
        }
    }
    
    public void eliminarAlumno(Alumno alumno){
        alumnoDao.eliminar(alumno);
    }
    
    public Alumno encontrarAlumno(Alumno alumno){
        return alumnoDao.buscarPorId(alumno);
    }
    
}
