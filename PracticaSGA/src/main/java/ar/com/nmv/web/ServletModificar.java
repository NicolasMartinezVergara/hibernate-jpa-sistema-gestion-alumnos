
package ar.com.nmv.web;

import ar.com.nmv.domain.Alumno;
import ar.com.nmv.servicio.ServicioAlumno;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet("/ServletModificar")
public class ServletModificar extends HttpServlet{
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String idAlumnoS = request.getParameter("idAlumno");
        Integer idAlumno = Integer.parseInt(idAlumnoS);
        
        Alumno alumno = new Alumno();
        alumno.setIdAlumno(idAlumno);
        
        ServicioAlumno servicioAlumno = new ServicioAlumno();
        alumno = servicioAlumno.encontrarAlumno(alumno);
        
        HttpSession sesion = request.getSession();
        sesion.setAttribute("alumno", alumno);
        
        request.getRequestDispatcher("/WEB-INF/modificarAlumno.jsp").forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        
        ServicioAlumno servicioAlumno = new ServicioAlumno();
        
        String modificar = request.getParameter("Modificar");
        
        if(modificar != null){
            
            String nombre = request.getParameter("nombre");
            String apellido = request.getParameter("apellido");
            String calle = request.getParameter("calle");
            String nroCalle = request.getParameter("nroCalle");
            String pais = request.getParameter("pais");
            String email = request.getParameter("email");
            String telefono = request.getParameter("telefono");
            
            HttpSession sesion = request.getSession();
            Alumno alumno = (Alumno) sesion.getAttribute("alumno");
            
            alumno.setNombre(nombre);
            alumno.setApellido(apellido);
            alumno.getDomicilio().setCalle(calle);
            alumno.getDomicilio().setNroCalle(nroCalle);
            alumno.getDomicilio().setPais(pais);
            alumno.getContacto().setEmail(email);
            alumno.getContacto().setTelefono(telefono);
            
            servicioAlumno.guardarAlumno(alumno);
            
            sesion.removeAttribute("alumno");
            
        } else {
            HttpSession sesion = request.getSession();
            Alumno alumno = (Alumno) sesion.getAttribute("alumno");
            servicioAlumno.eliminarAlumno(alumno);
            
        }
        
        response.sendRedirect("index.jsp");
    }
    
}
