
package ar.com.nmv.test;

import ar.com.nmv.dao.AlumnoDao;
import ar.com.nmv.dao.AsignacionDao;
import ar.com.nmv.dao.ContactoDao;
import ar.com.nmv.dao.CursoDao;
import ar.com.nmv.dao.DomicilioDao;
import java.util.List;


public class TestDao {
    
    public static void main(String[] args) {
        System.out.println("Alumnos: ");
        AlumnoDao alumnoDao = new AlumnoDao();
        imprimir(alumnoDao.listar());
        
        System.out.println("Domicilios");
        DomicilioDao domicilioDao = new DomicilioDao();
        imprimir(domicilioDao.listar());
        
        System.out.println("Contactos");
        ContactoDao contactoDao = new ContactoDao();
        imprimir(contactoDao.listar());
        
        System.out.println("Cursos");
        CursoDao cursoDao = new CursoDao();
        imprimir(cursoDao.listar());
        
        System.out.println("Asignaciones: ");
        AsignacionDao asignacionDao = new AsignacionDao();
        imprimir(asignacionDao.listar());
        
    }
    
    
    private static void imprimir(List coleccion){
        for(Object o : coleccion){
            System.out.println("Registro = " + o);
        }
    }
    
}
