

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>

        <title>Listado de Alumnos</title>
    </head>
    <body>
        <h1>Listado de Alumnos</h1>
        <br/>
        <a href="${pageContext.request.contextPath}/ServletRedireccionar">Agregar Alumno</a>
        <br/>


        <table border="1">
            <tr>
                <th>ID Alumno</th>
                <th>Nombre</th>
                <th>Domicilio</th>
                <th>Email</th>
                <th>Telefono</th>
            </tr>
            <c:forEach var="alumno" items="${alumnos}" varStatus="status">
                <tr>
                    <td>
                        <a href="${pageContext.request.contextPath}/ServletModificar?idAlumno=${alumno.idAlumno}">

                            ${status.count}
                        </a>

                    </td>
                    <td>${alumno.nombre} ${alumno.apellido}</td>
                    <td>${alumno.domicilio.calle} ${alumno.domicilio.nroCalle} ${alumno.domicilio.pais}</td>
                    <td>${alumno.contacto.email}</td>
                    <td>${alumno.contacto.telefono}</td>
                </tr>

            </c:forEach>

        </table>
    </body>
</html>
